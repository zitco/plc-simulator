let variable = 0.05;

function get() {
	return variable;
}

function set(value) {
	variable = parseFloat(value);
}

module.exports = { 
	name: "Value03",
	dataType: "Double",
	get,
	set 
};
