const hi = require('./value01').get();
const rect = require('./rect01');

let val = 0.0;
let x = 0;
let rr = rect.get();
let last = 0;

setInterval( () => {  
	let r = rect.get();
	if ( r != rr ) {
		x = 0.0;
		rr = r;
		last = val;
	}
	
	let e1 = parseFloat(Math.exp(-1*x));

	if (r == 0) {
		val = parseFloat(r + (last * e1));
	} else {
		val = parseFloat(r - ((hi-last) * e1));
	}

	x += 0.01;


}, 10);

function get() {
	return val;
}

module.exports = { 
	name: "Soft-01",
	dataType: "Double",
	get 
};
