let variable = 2;

function get() {
	return variable;
}

function set(value) {
	variable = parseFloat(value);
}

module.exports = { 
	name: "Value02",
	dataType: "Double",
	get,
	set 
};
