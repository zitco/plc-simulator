const value1 = require('./value01');
const value2 = require('./value02');

let noise = 0;

// emulate variable1 changing every 500 ms
setInterval( () => {  
	noise = Math.random() * value2.get();
}, 10);

function get() {
	return value1.get() + noise;
}

module.exports = { 
	name: "Noise-01",
	dataType: "Double",
	get 
};
