let variable = 1;

// emulate variable1 changing every 500 ms
setInterval( () => {  
	variable += 1 / (2*Math.PI); 
}, 10);

function get() {
	return Math.sin(variable);
}

module.exports = { 
	name: "Sinus",
	dataType: "Double",
	get 
};
