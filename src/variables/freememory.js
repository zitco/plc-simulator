const os = require("os");

function get() {
    return percentageMemUsed = os.freemem() / os.totalmem() * 100.0;
}

module.exports = { 
	name: "FreeMemory",
	dataType: "Double",
	get 
};
