let variable = 10.0;

function get() {
	return variable;
}

function set(value) {
	variable = parseFloat(value);
}

module.exports = { 
	name: "Value01",
	dataType: "Double",
	get,
	set 
};
