let variable = 2000;

function get() {
	return variable;
}

function set(value) {
	variable = parseFloat(value);
}

module.exports = { 
	name: "Value04",
	dataType: "Double",
	get,
	set 
};
