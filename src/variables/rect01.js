const value1 = require('./value01');
const value2 = require('./value02');
const value4 = require('./value04');

let on = true;

// emulate variable1 changing every 500 ms
function toggle() {
	setTimeout( () => {  
		on = !on;
		toggle();
	}, value4.get() );
}

function get() {
	return ( on ? value1.get() : 0 );
}

toggle();

module.exports = { 
	name: "Rect-01",
	dataType: "Double",
	get 
};
