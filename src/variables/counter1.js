let variable = 1;

// emulate variable1 changing every 500 ms
setInterval( () => {  
	variable += 1; 
}, 500);

function get() {
	return variable;
}


module.exports = { 
	name: "Counter-500ms",
	dataType: "Double",
	get 
};
