let noise = 0;

// emulate variable1 changing every 500 ms
setInterval( () => {  
	noise = Math.random() * 2;
}, 10);

function get() {
	return 1 - noise;
}

module.exports = { 
	name: "Noise-02",
	dataType: "Double",
	get 
};
