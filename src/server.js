/*global require,setInterval,console */
const opcua = require("node-opcua");
const variables = require('./variables');

// Let's create an instance of OPCUAServer
const server = new opcua.OPCUAServer({
    port: 4840, // the port of the listening socket of the server
    resourcePath: "/", // this path will be added to the endpoint resource name
     buildInfo : {
        productName: "PLC Simulator",
        buildNumber: "7658",
        buildDate: new Date(2020,6,22)
    }
});

function addVariables(namespace, device) {
    variables.forEach( variable => {

		let opcuavar = {
            componentOf: device,
            browseName: variable.name,
            dataType: variable.dataType,
            value: {}
        }

        if (variable.get) {
        	opcuavar.value.get = () => {
                return new opcua.Variant({dataType: opcua.DataType.Double, value: variable.get() });
            }
        }

        if (variable.set) {
        	opcuavar.value.set = (variant) => {
        		variable.set(variant.value);
                return opcua.StatusCodes.Good;
            }
        }

        namespace.addVariable(opcuavar);
    });	
}

function post_initialize() {
    console.log("initialized");
    function construct_my_address_space(server) {
    
        const addressSpace = server.engine.addressSpace;
        const namespace = addressSpace.getOwnNamespace();
    
        // declare a new object
        const deviceSet = namespace.addObject({
            organizedBy: addressSpace.rootFolder.objects,
            browseName: "DeviceSet"
        });
    
        const machine1 = namespace.addObject({
            organizedBy: deviceSet,
            browseName: "Machine-01"
        });
    
        const machine2 = namespace.addObject({
            organizedBy: deviceSet,
            browseName: "Machine-02"
        });

        const machine3 = namespace.addObject({
            organizedBy: deviceSet,
            browseName: "Machine-03"
        });
    
    	addVariables(namespace, machine1);
    	addVariables(namespace, machine2);
    	addVariables(namespace, machine3);

    }
    construct_my_address_space(server);
    server.start(function() {
        console.log("Server is now listening ... ( press CTRL+C to stop)");
        console.log("port ", server.endpoints[0].port);
        const endpointUrl = server.endpoints[0].endpointDescriptions()[0].endpointUrl;
        console.log(" the primary server endpoint url is ", endpointUrl );
    });
}

server.initialize(post_initialize);
