FROM node:12-slim

RUN apt -y update && apt -y install openssl

WORKDIR /app
COPY ./package.json /app/package.json
RUN npm install --unsafe-perm

COPY src /app

EXPOSE 4840

CMD npm start
